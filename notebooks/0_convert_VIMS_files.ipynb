{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Convert VIMS data file to conventional file format"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Calibrated data files from the Cassini [Visible and Infrared Mapping Spectrometer](https://solarsystem.nasa.gov/missions/cassini/mission/spacecraft/cassini-orbiter/visible-and-infrared-mapping-spectrometer/) (VIMS) are available from the [Cassini VIMS Data Portal](https://vims.univ-nantes.fr/) (offered by Université de Nantes, data and images courtesy of NASA/Caltech-JPL/University of Arizona/LPG Nantes.).<sup>&dagger;</sup>\n",
    "\n",
    "\n",
    "\n",
    "The VIMS data files from the Cassini mission can be downloaded and read using the [pyvims](https://github.com/seignovert/pyvims) module.\n",
    "In this notebook, we will convert a Cassini data file from the originial \".cube\" format into a more common format, HDF5.\n",
    "The HDF5 file is included in the repository, so if you are having trouble installing pyvims, you can skip this notebook.\n",
    "\n",
    "We are going to convert the measurement labelled `C1554970778_1` from a fly-by maneuver targeting the Saturn moon <a href=https://en.wikipedia.org/wiki/Titan_%28moon%29>Titan</a>. A preview of the data is available [here](https://vims.univ-nantes.fr/cube/1554970778_1).\n",
    "\n",
    "If you want to use other data files, you can download them as shown in input cell `[2]`.\n",
    "\n",
    "---\n",
    "\n",
    "<sup>&dagger;</sup>Le Mouélic et al., The Cassini VIMS archive of Titan: From browse products to global infrared color maps, *Icarus* **319** (2019), [DOI: 10.1016/j.icarus.2018.09.017](https://doi.org/10.1016/j.icarus.2018.09.017)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import datetime\n",
    "import h5py\n",
    "import json\n",
    "import matplotlib.pyplot as plt\n",
    "import os\n",
    "import pyvims\n",
    "\n",
    "from hashlib import sha1\n",
    "from pathlib import Path\n",
    "from scripts.context import PROJ_DIR\n",
    "from tempfile import TemporaryDirectory"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Convert spectral data to HDF5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We download the dataset by refering to its ID:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tmp = TemporaryDirectory()\n",
    "# we don't keep the original data file, so we download\n",
    "# it into a temporary directory (root=...)\n",
    "C1554970778_1 = pyvims.VIMS('C1554970778_1', root=tmp.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A preview of the data at a specific wavelength (here 1.57 µm) is available like so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(C1554970778_1@1.57);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We declare some meta data that we want to store in our HDF5 file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "meta = {\n",
    "    \"size\":    (C1554970778_1.NP, C1554970778_1.NL, C1554970778_1.NS),\n",
    "    \"channel\":  C1554970778_1.channel,\n",
    "    \"mode\":     C1554970778_1.mode,\n",
    "    \"start\":    C1554970778_1.start.isoformat(),\n",
    "    \"stop\":     C1554970778_1.stop.isoformat(),\n",
    "    \"exposure\": C1554970778_1.expo,\n",
    "    \"target\":   C1554970778_1.target_name,\n",
    "    \"flyby\":    C1554970778_1.flyby.name\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we write the (meta) data to disk in HDF5 format (if the file does not yet exist):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fp = Path(os.path.join(PROJ_DIR, \"data\", \"converted\", \"C1554970778_1.h5\"))\n",
    "\n",
    "if not fp.exists():\n",
    "    h5file = h5py.File(fp, 'w')\n",
    "\n",
    "    # Numpy arrays can be stored using the `create_dataset()` method:\n",
    "    h5file.create_dataset('wavelengths', data=C1554970778_1.wvlns)\n",
    "    spectra_ds = h5file.create_dataset('spectra', data=C1554970778_1.data)\n",
    "\n",
    "    # Meta data can be attached via the `attrs` attribute of a dataset:\n",
    "    for k, v in meta.items():\n",
    "        spectra_ds.attrs[k] = v\n",
    "        \n",
    "    h5file.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculate checksum"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are going to calculate a checksum (hash) for the file we created.\n",
    "In this way, we can later check that our data file is exactly the way we created it here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Calculating a hash value, e.g. in hexadecimal format, works like so:\n",
    "\n",
    "```\n",
    "sha1(<raw bytes string>).hexdigest()\n",
    "```\n",
    "\n",
    "Of course, other hashing algorithms like md5 or sha256 are also available."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(fp, 'rb') as f:\n",
    "    print(sha1(f.read()).hexdigest())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; to top](#Convert-VIMS-data-files-to-conventional-file-formats) | [next notebook &rarr;](1_data_exploration.ipynb)"
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "sample-project",
   "language": "python",
   "name": "sample-project"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
