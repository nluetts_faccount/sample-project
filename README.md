# Sample project

Author: N. Luettschwager

Start date: 2019-09-26

Issued: 2021-03-03

## Introduction

This sample project is part of the course [Data Evaluation with Python: Tools and Good Practice](https://hbond.uni-goettingen.de/ma_chem/epc/mc.html#dep).
The goal of this project is to show how to organise Python code
and Jupyter notebooks in a way that is easy to understand by others and easy
to install and run. It includes several common files (such as `requirements.txt`,
`README.md`, `LICENSE`) and tries to follow good practice recommendations closely to facilitate reproducability and to enable easy sharing.

In the project, we analyze data from the Cassini mission. Spatially resolved spectra
of the Saturn moon [Titan](https://en.wikipedia.org/wiki/Titan_%28moon%29), recorded using the [VIMS](https://solarsystem.nasa.gov/missions/cassini/mission/spacecraft/cassini-orbiter/visible-and-infrared-mapping-spectrometer/)
instrument, will be analysed and we will show that we can seperate spectra characteristic of the atmosphere from spectra characteristic for light reflected from the surface by statistical analysis.

By doing this we will touch the following topics:

- file I/O with files in the HDF5 format, hashing files
  ([notebooks/0_convert_VIMS_files.ipynb](notebooks/0_convert_VIMS_files.ipynb))
- interactive data visualization using
  [Bokeh](https://docs.bokeh.org/en/latest/)
  and [widgets](https://ipywidgets.readthedocs.io/en/stable/)
  ([notebooks/1_data_exploration.ipynb](notebooks/1_data_exploration.ipynb))
- finding outliers in the data by statistical means using [Numpy](https://numpy.org/)
  ([notebooks/2_preprocessing.ipynb](notebooks/2_preprocessing.ipynb))
- finding different types of spectra using statistical tools from the
  [scikit-learn package](https://scikit-learn.org/stable/index.html)
  (k-means clustering, principle component analysis,
  [notebooks/3_classification.ipynb](notebooks/3_classification.ipynb))
- writing a report in Jupyter and exporting it to Word or LaTeX using
  [nbconvert](https://nbconvert.readthedocs.io/en/latest/) and
  [pandoc](https://pandoc.org)
  ([notebooks/4_report.ipynb](notebooks/4_report.ipynb))

Note that while this project shall serve as an example how a "real" research project based on Python/Jupyter may be organized, the notebooks are written in an "educational style", meaning that in a research project there would be more comments on the actual data than on the mechanics of programming.

In the course, we will not go through the code in great detail (you can read through it and send me some questions), but are more concerned with how the files are organized and certain things are achieved, e.g.:

- sharing code among notebooks by using a custom module (`notebooks/scripts`)
- defining short code snippets that can be loaded into a notebook (`notebooks/snippets`)
- testing code and notebooks (`test`)

If you have ideas on how to improve the sample project, please [open an issue on gitlab.gwdg.de](https://gitlab.gwdg.de/jupyter-course/sample-project/-/issues).

## Dependencies

I assume you install and run the project on GWDG's JupyterHub service, [https://jupyter-cloud.gwdg.de](https://jupyter-cloud.gwdg.de), which comes with basic dependencies pre-installed and runs JupyterLab (instead of Jupyter Notebook). If you prefer your own comupter, you have to install Python and JupyterLab locally, e.g. by downloading Python from https://www.python.org/downloads/ and [installing JupyterLab via pip](https://jupyterlab.readthedocs.io/en/latest/getting_started/installation.html#installation). You will further have to install:

- [Git](https://git-scm.com/downloads)
- [`ipywidgets` extension for JupyterLab](https://ipywidgets.readthedocs.io/en/7.6.3/user_install.html)
- [`bokeh` extension for JupyterLab](https://docs.bokeh.org/en/latest/docs/user_guide/jupyter.html?highlight=jupyterlab#jupyterlab)
- [pandoc](https://pandoc.org/installing.html) (required only for document conversion)

## Downloading the project

Clone the project *via* git.
Type in the terminal (command line):

```bash
git clone https://gitlab.gwdg.de/jupyter-course/sample-project.git --depth=1
```

You can also [download the project as a zip file](https://gitlab.gwdg.de/jupyter-course/sample-project/-/archive/master/sample-project-master.zip) and extract it to the location of your choice.

Enter the newly created directory:

```bash
cd sample-project
```

## Installing the environment

We will use a dedicated environment for our project, as we should for every project.
The sample project includes a `requirements.txt` file which lists all the Python
packages and versions that we need to install to run the code of the project.
The installation process is layed out in the following. Again, the commands are to be typed into the terminal (command line).

Make a dedicated Python environment:

```bash
python3 -m venv env
```

Activate the environment:

```bash
source env/bin/activate
```

Update `pip` and install `wheel`, a tool that helps installing other packages:

```bash
(env) pip install -U pip wheel
```

(Note that `(env)` indicates that the environment *env* is active.)

Install the required packages:

```bash
(env) pip install -r requirements.txt
```

Register a new Python kernel with Jupyter:

```bash
(env) python -m ipykernel install --name sample-project --user
```

## Testing the project

To test that your installation runs as expected, run:

```bash
(env) python test/test_data.py
(env) python test/test_notebooks.py
```

## Uninstalling the environment

You can remove the project's environment as follows:


Delete the `env` folder inside project directory:

```bash
rm -r env
```

Uninstall the Jupyter kernel:

```bash
jupyter kernelspec remove sample-project
```

## Converting documents

To convert documents, you can simply click *File*  *Download as*
(Jupyter notebook) or *File*  *Export Notebook As ...* (JupyterLab).
This will run the tool [nbconvert](https://nbconvert.readthedocs.io/en/latest/)
to produce the file that you want (depending on the export format, more dependencies may need to be installed).

You have more control over the process if you run `nbconvert` in the terminal.
For example, you can suppress code input cells with the `--no-input` flag:

```bash
jupyter nbconvert 4_report.ipynb --to=html --no-input
```

A good starting point to convert to docx or tex is Markdown:

```bash
jupyter nbconvert 4_report.ipynb --to=markdown --no-input
```

Subsequently, you can use [pandoc](https://pandoc.org) to create a Word
document ...

```bash
pandoc -i 4_report.md -o 4_report.docx
```

... or a LaTeX document:

```bash
pandoc -i 4_report.md -o 4_report.tex
```

Both variants will need some touch up.
You can find results of some conversions in the folder `notebooks/converted`.
